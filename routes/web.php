<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Auth::routes();
Route::get('/', 'ItemController@index');
Route::get('/home', 'ItemController@index')->name('home');
Route::get('/reading_list', 'ListController@reading');
Route::get('/learning_list', 'ListController@learning');
Route::get('items/start', 'ItemController@start');
Route::get('/items/{id}/delete', 'ItemController@delete');
Route::get('/items/maintain', 'ItemController@maintain');
Route::resource('items', 'ItemController');
Route::get('/categories/{id}/delete', 'CategoryController@delete');
Route::resource('categories', 'CategoryController');
