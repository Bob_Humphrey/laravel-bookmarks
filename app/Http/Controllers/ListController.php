<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Illuminate\Support\Facades\Log;

class ListController extends Controller
{
  public function reading()
  {
    $items = Item::where('reading_list', 1)
      ->orderBy('id', 'DESC')->get();
    Log::info($items);
    return view('list.reading', [
      'items' => $items
    ]);
  }

  public function learning()
  {
    $items = Item::where('learning_list', 1)
      ->orderBy('id', 'DESC')->get();
    return view('list.learning', [
      'items' => $items
    ]);
  }
}
