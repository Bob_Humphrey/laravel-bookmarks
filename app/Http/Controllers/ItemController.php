<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Item;
use stdClass;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth', ['only' => ['store', 'update', 'destroy']]);
  }

  private function newList()
  {
    $list = collect();
    $item = new stdClass();
    $item->name = "Never On List";
    $list->push($item);
    $item = new stdClass();
    $item->name = "Currently On List";
    $list->push($item);
    $item = new stdClass();
    $item->name = "Previously On List";
    $list->push($item);
    return $list;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::with('items')->orderBy('name')->get();
    return view('item.index', [
      'categories' => $categories
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::orderBy('name')->get();
    $readingList = $this->newList();
    $learningList = $this->newList();
    return view('item.create', [
      'categories' => $categories,
      'readingList' => $readingList,
      'learningList' => $learningList
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate(request(), [
      'category_id' => 'exists:categories,id',
      'title' => 'required',
      'url' => 'required|url'
    ]);
    Item::create(request(['category_id', 'title', 'url', 'reading_list', 'learning_list']));
    $request->session()->flash('status',  request()->title . ' has been added.');
    return redirect(action('ItemController@index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $item = Item::find($id);
    $itemCategory = Category::find($item->category_id);
    $categories = Category::orderBy('name')->get();
    $readingList = $this->newList();
    $learningList = $this->newList();
    return view('item.show', [
      'item' => $item,
      'itemCategory' => $itemCategory,
      'categories' => $categories,
      'readingList' => $readingList,
      'learningList' => $learningList
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $item = Item::find($id);
    $categories = Category::orderBy('name')->get();
    $readingList = $this->newList();
    $learningList = $this->newList();
    return view('item.edit', [
      'item' => $item,
      'categories' => $categories,
      'readingList' => $readingList,
      'learningList' => $learningList
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate(request(), [
      'category_id' => 'exists:categories,id',
      'title' => 'required',
      'url' => 'required|url'
    ]);
    Log::info($request);
    Log::info($id);
    $item = Item::find($id);
    $item->category_id = request()->category_id;
    $item->title = request()->title;
    $item->url = request()->url;
    $item->reading_list = request()->reading_list;
    $item->learning_list = request()->learning_list;
    $item->save();
    $request->session()->flash('status', $item->title . ' has been updated.');
    return redirect(action('ItemController@maintain'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $item = Item::find($id);
    $title = $item->title;
    $item->delete();
    session()->flash('status', $title . ' has been deleted.');
    return redirect(action('ItemController@maintain'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @return \Illuminate\Http\Response
   */
  public function maintain()
  {
    $categories = Category::with('items')->orderBy('name')->get();
    $itemCount = Item::get()->count();
    return view('item.maintain', [
      'categories' => $categories,
      'itemCount' => $itemCount,
    ]);
  }

  /**
   * Display the specified resource to be deleted.
   * An extra step to protect against accidental deletes.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete($id)
  {
    $item = Item::find($id);
    $itemCategory = Category::find($item->category_id);
    $categories = Category::orderBy('name')->get();
    $readingList = $this->newList();
    $learningList = $this->newList();
    return view('item.delete', [
      'item' => $item,
      'itemCategory' => $itemCategory,
      'categories' => $categories,
      'readingList' => $readingList,
      'learningList' => $learningList
    ]);
  }


  public function start(Request $request)
  {
    $categories = Category::orderBy('name')->get();
    $readingList = $this->newList();
    $learningList = $this->newList();
    return view('item.create', [
      'categories' => $categories,
      'startUrl' => request()->url,
      'startTitle' => request()->title,
      'readingList' => $readingList,
      'learningList' => $learningList
    ]);
    //return 'Hello World';
  }
}
