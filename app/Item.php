<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  protected $fillable = [
    'category_id',
    'title',
    'url',
    'reading_list',
    'learning_list'
  ];

  public function category()
  {
    return $this->belongsTo('App\Category');
  }
}
