@php
use Illuminate\Support\Facades\Log;
$categoryCount = $categories->count();
$columns = 3;
$categoriesPerColumn = ceil($categoryCount / $columns);
@endphp

@extends('layouts.app')

@section('content')

<div class="flex text-blue-800 justify-center pb-2">
  <h2 class="text-3xl font-inter_semibold pt-2 pb-8">Edit Categories</h2>
</div>
<div class="lg:flex font-inter_medium  text-gray-800">
  @for ($column = 0; $column < $columns; $column++) <div class="w-full lg:w-1/3 px-1">
    <table class="w-full border-b border-gray-400">
      @php
      $firstCategory = $column * $categoriesPerColumn;
      $lastCategory = ($column + 1) * $categoriesPerColumn;
      @endphp

      @foreach($categories as $category)

      @if ($loop->index >= $firstCategory && $loop->index < $lastCategory) <tr>
        <td class="border-t border-l border-r border-gray-400 px-2 py-2 w-3/5">{{ $category->name }}</td>
        <td class="justify-end w-1/3 border-t border-r border-gray-400 px-1 py-2">
          <div class="flex justify-center">
            <a class="cursor-pointer px-2" href="{{ url('categories/' . $category->id . '/edit') }}">
              @svg('edit-pencil', 'fill-current h-5 w-5')
            </a>
            <a class="cursor-pointer px-2" href="{{ url('categories/' . $category->id . '/delete') }}">
              @svg('close-outline', 'fill-current h-5 w-5')
            </a>
          </div>
        </td>
        </tr>
        @endif

        @endforeach
    </table>
</div>
@endfor
</div>

@endsection