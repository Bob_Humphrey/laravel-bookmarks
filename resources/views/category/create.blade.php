@extends('layouts.app')

@section('content')

@php
$urlStore = action('CategoryController@store');
$name = old('name');
$disabled = '';
@endphp

<h2 class="text-3xl text-blue-800 font-inter_semibold text-center pt-2 pb-4">Add Category</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">


    @include('category.form', [
    'parent' => 'create',
    'formMethod' => 'POST',
    'formAction' => $urlStore
    ])

  </div>
</div>

@endsection