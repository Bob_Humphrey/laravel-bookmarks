<form class="w-full pt-4" method="{{ $formMethod }}" action="{{ $formAction }}">

  {{ csrf_field() }}

  @if ($parent === 'edit')
  <input name="_method" type="hidden" value="PATCH">
  @elseif ($parent === 'delete')
  <input name="_method" type="hidden" value="DELETE">
  @endif

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="name">
        Name
      </label>
    </div>
    <div class="lg:w-3/4">
      <input class="form-input" id="name" name="name" type="text" value="{{ $name }}" {{ $disabled }}>
    </div>
  </div>

  <div class="lg:flex justify-center">

    @if ($parent === 'create')
    <input class="form-button" type="submit" name="Add" value="Add">
    @elseif ($parent === 'edit')
    <input class="form-button" type="submit" name="Update" value="Update">
    @elseif ($parent === 'delete')
    <input class="form-button" type="submit" name="Delete" value="Delete">
    @endif

  </div>
</form>