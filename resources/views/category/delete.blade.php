@extends('layouts.app')

@section('content')

@php
$urlActionDestroy = url('categories/' . $category->id);
$name = $category->name;
$disabled = 'disabled';
@endphp

<h2 class="text-3xl text-blue-800 font-inter_semibold text-center pt-2 pb-4">Delete Category</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">
    <div class="text-white bg-red-600 py-4 px-2 mb-2 rounded">
      Warning! This action cannot be undone.
    </div>

    @include('category.form', [
    'parent' => 'delete',
    'formMethod' => 'POST',
    'formAction' => $urlActionDestroy
    ])

  </div>
</div>

@endsection