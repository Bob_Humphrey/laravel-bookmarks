@extends('layouts.app')

@section('content')

@php
use Illuminate\Support\Facades\Log;
$urlActionUpdate = url('items/' . $item->id);
$title = (count($errors)) ? old('title') : $item->title;
$url = (count($errors)) ? old('url') : $item->url;
$reading_list = (count($errors)) ? old('reading_list') : $item->reading_list;
$learning_list = (count($errors)) ? old('learning_list') : $item->learning_list;
$categoryId = (count($errors)) ? old('category_id') : $item->category_id;
foreach ($categories as $category) {
$category->selected = ($category->id == $categoryId) ? 'selected' : '';
}
$loopIndex = 0;
foreach ($readingList as $readingListItem) {
$readingListItem->selected = ($reading_list == $loopIndex) ? 'selected' : '';
$loopIndex++;
}
$loopIndex = 0;
foreach ($learningList as $learningListItem) {
$learningListItem->selected = ($learning_list == $loopIndex) ? 'selected' : '';
$loopIndex++;
}
$disabled = '';
@endphp

<h2 class="text-3xl text-blue-800 font-inter_semibold text-center pt-2 pb-4">Edit Bookmark</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">

    @include('item.form', [
    'parent' => 'edit',
    'formMethod' => 'POST',
    'formAction' => $urlActionUpdate
    ])

  </div>
</div>

@endsection