@extends('layouts.app')

@section('content')

@php
$urlStore = action('ItemController@store');
$title = old('title');
$url = old('url');
$reading_list = (count($errors)) ? old('reading_list') : 0;
$learning_list = (count($errors)) ? old('learning_list') : 0;
if (!empty($startUrl)) {$url = $startUrl;}
if (!empty($startTitle)) {$title = $startTitle;}
// Determine if one of the categories was previously selected.
$selectCategories = $categories->map(function ($category, $key) {
$category->selected = (old('category_id') == $category->id) ? 'selected' : '';
return $category;
});
$loopIndex = 0;
foreach ($readingList as $readingListItem) {
$readingListItem->selected = ($reading_list == $loopIndex) ? 'selected' : '';
$loopIndex++;
}
$loopIndex = 0;
foreach ($learningList as $learningListItem) {
$learningListItem->selected = ($learning_list == $loopIndex) ? 'selected' : '';
$loopIndex++;
}
$disabled = '';
@endphp

<h2 class="text-3xl text-blue-800 font-inter_semibold text-center pt-2 pb-4">Add Bookmark</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">

    @include('item.form', [
    'parent' => 'create',
    'formMethod' => 'POST',
    'formAction' => $urlStore
    ])

  </div>
</div>

@endsection