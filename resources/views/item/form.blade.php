<form class="w-full pt-4" method="{{ $formMethod }}" action="{{ $formAction }}">

  {{ csrf_field() }}

  @if ($parent === 'edit')
  <input name="_method" type="hidden" value="PATCH">
  @elseif ($parent === 'delete')
  <input name="_method" type="hidden" value="DELETE">
  @endif

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="category_id">
        Category
      </label>
    </div>
    @if ($disabled)
    <div class="lg:w-3/4">
      <input class="form-input" id="category_id" name="category_id" type="text" value="{{ $itemCategory->name }}"
        {{ $disabled }}>
    </div>
    @else
    <div class="lg:w-3/4">
      <select class="form-input" id="category_id" name="category_id">
        @foreach ($categories as $category)
        <option value="{{ $category->id }}" {{ $category->selected }}>
          {{ $category->name }}
        </option>
        @endforeach
      </select>
    </div>
    @endif
  </div>

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="title">
        Title
      </label>
    </div>
    <div class="lg:w-3/4">
      <input class="form-input" id="title" name="title" type="text" value="{{ $title }}" {{ $disabled }}>
    </div>
  </div>

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="url">
        Url
      </label>
    </div>
    <div class="lg:w-3/4">
      <input class="form-input" id="url" name="url" type="text" value="{{ $url }}" {{ $disabled }}>
    </div>
  </div>

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="reading_list">
        Reading List
      </label>
    </div>
    @if ($disabled)
    <div class="lg:w-3/4">
      <input class="form-input" id="reading_list" name="reading_list" type="text"
        value="{{ $readingList[$item->reading_list]->name }}" {{ $disabled }}>
    </div>
    @else
    <div class="lg:w-3/4">
      <select class="form-input" id="reading_list" name="reading_list">
        @foreach ($readingList as $readingListItem)
        <option value="{{ $loop->index }}" {{ $readingListItem->selected }}>
          {{ $readingListItem->name }}
        </option>
        @endforeach
      </select>
    </div>
    @endif
  </div>

  <div class="lg:flex items-center mb-3">
    <div class="lg:w-1/4">
      <label class="form-label lg:text-right" for="learning_list">
        Learning List
      </label>
    </div>
    @if ($disabled)
    <div class="lg:w-3/4">
      <input class="form-input" id="learning_list" name="learning_list" type="text"
        value="{{ $learningList[$item->learning_list]->name }}" {{ $disabled }}>
    </div>
    @else
    <div class="lg:w-3/4">
      <select class="form-input" id="learning_list" name="learning_list">
        @foreach ($learningList as $learningListItem)
        <option value="{{ $loop->index }}" {{ $learningListItem->selected }}>
          {{ $learningListItem->name }}
        </option>
        @endforeach
      </select>
    </div>
    @endif
  </div>

  <div class="lg:flex justify-center">

    @if ($parent === 'create')
    <input class="form-button" type="submit" name="Add" value="Add">
    @elseif ($parent === 'show')
    <a class="form-button mr-6" href="{{ $urlActionEdit }}">Edit</a>
    <a class="form-button mr-6" href="{{ $urlActionDelete }}">Delete</a>
    <a class="form-button" href="{{ $urlWebsite }}" target="_blank" rel="noopener noreferrer">Website</a>
    @elseif ($parent === 'edit')
    <input class="form-button" type="submit" name="Update" value="Update">
    @elseif ($parent === 'delete')
    <input class="form-button" type="submit" name="Delete" value="Delete">
    @endif

  </div>

</form>