@php
use Illuminate\Support\Facades\Log;
$categoryCount = $categories->count();
$columns = 3;
$categoriesPerColumn = ceil($categoryCount / $columns);
@endphp


@extends('layouts.app')

@section('content')

<div class="flex text-blue-800 justify-center">
  <h2 class="text-3xl font-inter_semibold pt-2 pb-8">Edit Bookmarks</h2>
  <div class="show-all w-16 ml-4 cursor-pointer">
    @svg('chevron-down', 'fill-current')
  </div>
  <div class="hide-all w-16 ml-4 cursor-pointer">
    @svg('chevron-up', 'fill-current')
  </div>
</div>

<div class="lg:flex">
  @for ($column = 0; $column < $columns; $column++) <div class="w-full lg:w-1/3 px-1">
    @php
    $firstCategory = $column * $categoriesPerColumn;
    $lastCategory = ($column + 1) * $categoriesPerColumn;
    @endphp

    @foreach($categories as $category)

    @if ($loop->index >= $firstCategory && $loop->index < $lastCategory) @php $items=$category->items->sortBy('title');
      @endphp

      <div class="pr-6">
        <h3 class="category font-inter_semibold text-xl text-blue-600 cursor-pointer py-1">
          {{ $category->name }}
        </h3>
        <div class="items hidden">
          <table class="w-full border-b border-gray-400 mb-4">
            @foreach($items as $item)
            <tr class="flex justify-between text-gray-800">
              <td class="justify-start w-2/3 border-t border-l border-r border-gray-400 px-1 py-2">
                <a class="font-inter_medium hover:text-blue-600 no-underline" href="{{ $item->url }}" target="_blank">
                  {{ Str::of($item->title)->limit(25) }}
                </a>
              </td>
              <td class="justify-end w-1/3 border-t border-r border-gray-400 px-1 pt-3">
                <div class="flex justify-center">
                  <a class="cursor-pointer px-2" href="{{ url('items/' . $item->id) }}">
                    @svg('view-show', 'fill-current h-5 w-5')
                  </a>
                  <a class="cursor-pointer px-2" href="{{ url('items/' . $item->id . '/edit') }}">
                    @svg('edit-pencil', 'fill-current h-5 w-5')
                  </a>
                  <a class="cursor-pointer px-2" href="{{ url('items/' . $item->id . '/delete') }}">
                    @svg('close-outline', 'fill-current h-5 w-5')
                  </a>
                </div>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>

      @php
      $categoryCount++;
      @endphp
      @endif

      @endforeach

</div>
@endfor

</div>

@endsection