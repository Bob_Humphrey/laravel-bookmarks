@extends('layouts.app')

@section('content')

@php
$urlActionDelete = url('items/' . $item->id . '/delete');
$urlActionEdit = url('items/' . $item->id . '/edit');
$urlActionShow = url('items/' . $item->id);
$urlWebsite = $item->url;
$title = $item->title;
$url = $item->url;
$reading_list = $item->reading_list;
$learning_list = $item->learning_list;
$disabled = 'disabled';
@endphp

<h2 class="text-3xl text-blue-800 font-inter_semibold text-center pt-2 pb-4">Show Bookmark</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">


    @include('item.form', [
    'parent' => 'show',
    'formMethod' => 'GET',
    'formAction' => $urlActionShow
    ])

  </div>
</div>

@endsection