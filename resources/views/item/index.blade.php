@php
$categoryCount = $categories->count();
$columns = 4;
$categoriesPerColumn = ceil($categoryCount / $columns);
@endphp

@extends('layouts.app')

@section('content')

<div class="flex text-blue-800 justify-center">
  <h2 class="text-3xl font-inter_semibold pt-2 pb-8">Bookmarks</h2>
  <div class="show-all w-16 ml-4 cursor-pointer">
    @svg('chevron-down', 'fill-current')
  </div>
  <div class="hide-all w-16 ml-4 cursor-pointer">
    @svg('chevron-up', 'fill-current')
  </div>
</div>

<div class="lg:flex">

  @for ($column = 0; $column < $columns; $column++) <div class="w-full lg:w-1/4 px-1">

    @php
    $firstCategory = $column * $categoriesPerColumn;
    $lastCategory = ($column + 1) * $categoriesPerColumn;
    @endphp

    @foreach($categories as $category)

    @if ($loop->index >= $firstCategory && $loop->index < $lastCategory) @php $items=$category->items->sortBy('title');
      @endphp

      <div class="pr-6">
        <h3 class="category font-inter_semibold text-xl text-blue-600 cursor-pointer py-1">
          {{ $category->name }}
        </h3>

        <div class="items hidden">
          @foreach($items as $item)
          <div class="py-1">
            <a class="font-inter_medium text-gray-800 hover:underline no-underline" href="{{ $item->url }}"
              target="_blank" rel="noopener noreferrer">
              {{ $item->title }}
            </a>
          </div>
          @endforeach
        </div>
      </div>

      @php
      $categoryCount++;
      @endphp
      @endif

      @endforeach

</div>
@endfor

</div>

@endsection