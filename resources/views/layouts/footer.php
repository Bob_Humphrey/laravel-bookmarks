<footer className="flex w-full justify-center bg-blue-200 py-16">
    <div className="w-16">
        <a href="https://bob-humphrey.com">
            <img src="{{ asset('img/bh-logo.gif') }}" alt="Logo" />
        </a>
    </div>
</footer>