@php
$logoUrl = asset('img/bh-logo-oxblood.gif');
$cssTailwind = asset('css/tailwind.css');
$jsCustom = asset('js/custom.js');
$jsUmbrella = asset('js/umbrella.js');
@endphp

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script>   -->

  <!-- Styles -->
  <link rel="stylesheet" href="{{ $cssTailwind }}">

  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//analytics.bob-humphrey.com/";
      _paq.push(['setTrackerUrl', u+'matomo.php']);
      _paq.push(['setSiteId', '3']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->

</head>

<body>
  <div id="app">
    <nav class="xl:flex justify-between bg-blue-700 text-white px-10 py-3 mb-5">
      <h1 class="justify-start text-3xl font-inter_semibold pt-1">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
      </h1>
      <div class="justify-end">
        <ul class="lg:flex font-inter_regular lg:text-right">
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ action('ListController@reading') }}">Reading List</a>
          </li>
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ action('ListController@learning') }}">Learning List</a>
          </li>
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ action('ItemController@create') }}">Add Bookmark</a>
          </li>
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ action('ItemController@maintain') }}">Edit Bookmarks</a>
          </li>
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ url('/categories/create') }}">Add Category</a>
          </li>
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ url('/categories') }}">Edit Categories</a>
          </li>
          <!-- Authentication Links -->
          @guest
          <li class="lg:mx-2 py-2 lg:py-4">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          <!--
                    @if (Route::has('register'))
                    <li class="lg:mx-2 py-2 lg:py-4">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                    @endif
                    -->
          @else
          <!--
                    <li class="lg:mx-2 py-2 lg:py-4">
                        Logged in as {{ Auth::user()->name }}
                    </li>
                    -->
          <li class="lg:mx-2 py-2 lg:py-4">
            <div class="">
              <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
          @endguest
        </ul>
      </div>
    </nav>

    <main class="lg:flex justify-center w-full">
      <div class="w-full py-2 px-10">
        @include('layouts.flash')
        @include('layouts.errors')
        @yield('content')
      </div>
    </main>

    <footer class="flex w-full justify-center bg-blue-200 py-32 mt-16">
      <div class="w-16">
        <a href="https://bob-humphrey.com">
          <img src="{{ asset('img/bh-logo.gif') }}" alt="Logo" />
        </a>
      </div>
    </footer>
  </div>

  <script src="{{ $jsUmbrella }}"></script>
  <script src="{{ $jsCustom }}"></script>
</body>

</html>