@if (count($errors))

<div class="flex justify-center font-inter_medium">
  <div class="lg:w-3/5 xl:w-1/2 text-white bg-red-600 py-4 px-4 mb-2 rounded">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
</div>

@endif