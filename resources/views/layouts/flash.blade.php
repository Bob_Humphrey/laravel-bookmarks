@if ($flash = session('status'))

<div class="flex justify-center font-inter_medium">
  <div class="lg:w-3/5 xl:w-1/2 text-white bg-green-600 py-4 px-4 mb-2 rounded">
    {{ $flash }}
  </div>
</div>

@endif