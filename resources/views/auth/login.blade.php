@extends('layouts.app')

@section('content')



<h2 class="text-3xl text-blue-800 font-inter_semibold text-center py-2">Login</h2>
<div class="flex justify-center">
  <div class="form-container lg:w-3/5 xl:w-1/2">
    <form class="w-full pt-4" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}

      <div class="lg:flex items-center mb-3">
        <div class="lg:w-1/4">
          <label class="form-label lg:text-right" for="email">
            Email
          </label>
        </div>
        <div class="lg:w-3/4">
          <input class="form-input" id="email" type="email" name="email" value="{{ old('email') }}">
        </div>
      </div>

      <div class="lg:flex items-center mb-3">
        <div class="lg:w-1/4">
          <label class="form-label lg:text-right" for="password">
            Password
          </label>
        </div>
        <div class="lg:w-3/4">
          <input class="form-input" id="password" type="password" name="password">
        </div>
      </div>

      <div class="lg:flex justify-center">
        <input class="form-button" type="submit" name="Login" value="Login">
      </div>

    </form>

  </div>
</div>
@endsection