@extends('layouts.app')

@section('content')

<div class="lg:w-1/2 xl:w-2/5 mx-auto">

  <div class="flex text-blue-800 justify-center">
    <h2 class="text-3xl font-inter_semibold pt-2 pb-8">Learning List</h2>
  </div>

  <div class="">
    @foreach($items as $item)
    <div class="flex py-2">
      <a class="cursor-pointer pr-3" href="{{ url('items/' . $item->id . '/edit') }}">
        @svg('edit-pencil', 'fill-current h-5 w-5')
      </a>
      <a class="font-inter_medium text-gray-800 hover:underline no-underline" href="{{ $item->url }}" target="_blank"
        rel="noopener noreferrer">
        {{ $item->title }}
      </a>
    </div>
    @endforeach
  </div>

</div>

@endsection